<?php

use yii\db\Migration;

/**
 * Handles the creation of table `plan_properties`.
 */
class m180528_173709_create_plan_properties_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('plan_properties', [
            'property_id' => $this->primaryKey(),
            'property_type_id' => $this->integer(11)->notNull(),
            'active_from' => $this->string(11),
            'active_to' => $this->string(11)->null(),
            'plan_id' => $this->integer(11)->notNull(),
            'prop_value' => $this->text()
        ]);
        
        $this->createIndex("plan_id", "plan_properties", ["plan_id"]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('plan_properties');
    }
}
