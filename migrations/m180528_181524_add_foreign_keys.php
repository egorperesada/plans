<?php

use yii\db\Migration;

/**
 * Class m180528_181524_add_foreign_keys
 */
class m180528_181524_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey("property_has_one_plan", "plan_properties", ["plan_id"], "plans", ["plan_id"], "CASCADE", "NO ACTION");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("property_has_one_plan", "plan_properties");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180528_181524_add_foreign_keys cannot be reverted.\n";

        return false;
    }
    */
}
