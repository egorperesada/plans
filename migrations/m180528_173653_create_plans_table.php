<?php

use yii\db\Migration;

/**
 * Handles the creation of table `plans`.
 */
class m180528_173653_create_plans_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('plans', [
            'plan_id' => $this->primaryKey(),
            'plan_name' => $this->text(),
            'plan_group_id' => $this->integer(11),
            'active_from' => $this->string(11),
            'active_to' => $this->string(11)->null(),
            'company_id' => $this->integer(11)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('plans');
    }
}
