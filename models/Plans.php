<?php

namespace perec\plans\models;

use Yii;

/**
 * This is the model class for table "plans".
 *
 * @property int $plan_id
 * @property string $plan_name
 * @property int $plan_group_id
 * @property string $active_from
 * @property string $active_to
 * @property int $company_id
 *
 * @property PlanProperties[] $planProperties
 */
class Plans extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plans';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plan_name'], 'string'],
            [['plan_group_id'], 'integer'],
            [['company_id'], 'safe' ],
            [['active_from', 'active_to'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => 'Plan ID',
            'plan_name' => 'Plan Name',
            'plan_group_id' => 'Plan Group ID',
            'active_from' => 'Active From',
            'active_to' => 'Active To',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanProperties()
    {
        return $this->hasMany(PlanProperties::class, ['plan_id' => 'plan_id']);
    }

}
