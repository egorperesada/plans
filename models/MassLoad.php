<?php
/**
 * Created by eaperesada.
 * Date: 02.06.2018
 * Time: 11:33
 * Email: ea.peresada@gmail.com
 * Phone: +7 (922) 339 60 49
 * VK: https://vk.com/peresada
 * Project: domrutest
 */

namespace perec\plans\models;

use yii\db\ActiveRecord;

abstract class MassLoad {
    protected $model;
    protected $data;
    protected $duplicateKeys;
    
    public function __construct( ActiveRecord $model ) {
        $this->model = $model;
    }
    
    abstract public function validateRow( array $row );
    
    /**
     * Creating batchInsert command
     *
     * @param array $data
     * */
    public function batch( array $data ) {
        $this->data = $data;
        $tableName = $this->model::tableName();
        $columns = $this->model->attributes();
        $this->prepareBeforeBatch();
        // Для этой задачи я решил пользоваться по большей степени функционалом и возможностями БД,
        // чтобы не делать дополнительные проверки в коде, посчитал, что это приемлимо для данной задачи
        // Разумеется, есть и другие варианты решения
        \Yii::$app->db->createCommand( "SET FOREIGN_KEY_CHECKS = 0;" )->execute();
        $sql = \Yii::$app->db->createCommand()->batchInsert( $tableName , $columns , $this->data )->getSql();
        $sql .= $this->getOnDuplicateUpdateStatement();
        \Yii::$app->db->createCommand( $sql )->execute();
        \Yii::$app->db->createCommand( "SET FOREIGN_KEY_CHECKS = 1;" )->execute();
    }
    
    /**
     * Preparing values before batchInsert command.
     * Setting values to NULL if value is an array, value can be type of array, because of
     * converting data from XML
     *
     * @return $this
     * */
    protected function prepareBeforeBatch() {
        
        foreach ( $this->data as $key => $row ) {
            foreach ( $row as $column => $value ) {
                if ( is_array( $value ) ) {
                    $this->data[ $key ][ $column ] = NULL;
                }
            }
        }
        
        return $this;
    }
    
    /**
     * Create sql statement happened on duplicate constraint,
     * if duplicateKeys were not set up it may cause integrityConstraint violation
     * */
    private function getOnDuplicateUpdateStatement() {
        return is_array( $this->duplicateKeys ) ? " ON DUPLICATE KEY UPDATE " . implode( "," , $this->duplicateKeys ) : NULL;
    }
    
    /**
     * Sets what columns should be updated by new values if duplicate contsraint will happen
     *
     * @param array $columns Column names that should be updated
     * */
    protected function setOnDuplicateUpdateColumns( array $columns ) {
        $command = \Yii::$app->db;
        foreach ( $columns as $column ) {
            $this->duplicateKeys[] = $command->quoteColumnName( $column ) . " = values(" . $command->quoteColumnName( $column ) . ")";
        }
    }
    
    /**
     * Diff date-values, and return if this date is more current
     * @param string $date
     * @return bool;
     * */
    protected function dateValidate( string $date ) {
        $current = new \DateTime( "now" );
        $target = new \DateTime( $date );
        
        return $target > $current;
    }
}