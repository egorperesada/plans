<?php
/**
 * Created by eaperesada.
 * Date: 29.05.2018
 * Time: 21:44
 * Email: ea.peresada@gmail.com
 * Phone: +7 (922) 339 60 49
 * VK: https://vk.com/peresada
 * Project: domrutest
 */

namespace perec\plans\services;

use perec\plans\models\Plans;
use perec\plans\models\MassLoad;
use yii\db\ActiveRecord;

class PlanProperties extends MassLoad {
    
    public function __construct( ActiveRecord $model ) {
        parent::__construct( $model );
        $this->setOnDuplicateUpdateColumns( [ "prop_value" , "active_to" , "property_type_id"]);
    }
    
    /**
     * Validation rules for plan_properties.xml ROWS
     *
     * @param array $data
     *
     * @return bool
     * */
    public function validateRow( array $data ) {
        if ( ! is_array( $data[ "active_to" ] ) ) {
            return $this->dateValidate( $data[ "active_to" ] );
        }
        else {
            return true;
        }
    }
    
    
    /**
     * Delete all rows that have not related rows in `Plans` table
     * */
    public function cleanConstraintFailed() {
        $sql = "DELETE FROM `" . $this->model::tableName() . "`
                WHERE `plan_id` NOT IN
                ( SELECT `plan_id`
                FROM `" . Plans::tableName() . "` )";
        \Yii::$app->db->createCommand($sql)->execute();
    }
    
}