**Описание**

Это тестовый модуль для импорта файлов plans.xml и plan_properties.xml.

---
## Требования
1. версия yii2: 2.0.14
2. версия php: 7+
3. шаблон yii2 ориентировочно не имеет значения, но тестировался в yii2-app-advanced
4. mysql 5.6+

## Установка

Следуйте следующим пунктам для установки этого модуля

1. В консоли проекта yii2 исполните команду `composer require perec/plans "dev-master"` 
    - Либо добавьте в composer.json, в элемент **require** строку `"perec/plans": "dev-master"`, после чего обновите composer
3. В файле конфигурации **console/config/main.php** (для Advanced-версии) или **config/web.php** (для Basic-версии) добавьте текущий модуль
    - `'modules' => [
        "plans" => [
            "class" => "perec\plans\Module"
           ]
       ]`
4. После этого необходимо инициировать миграции для этого модуля. Выполните в консоли следующую команду: `yii migrate --migrationPath=@perec/plans/migrations`
5. Если все сделано корректно, при инициализации команды консоли `yii` в списке доступных команд должен отобразиться контроллер `-plans/Import`
6. Модуль установлен

---

## Использование и настройка файлов конфигурации модуля

По умолчанию модуль обрабатывает файлы **plans.xml** и **plan_properties.xml** в папке **/date** в пути расположения модуля. 

1. Если файлы должны располагаться в другом месте или они имеют иные названия, поменяйте файл **perec/plans/config.php** сохраняя структуру массива
    - `return [
    "files_dir" => "path/to/data/folder",
    "target_files" => [
        "plans" => "your_plans_filename.xml",
        "properties" => "your_plan_properties_filename.xml"]
    ];`
2. Обрабатываться будут только файлы **plans** и **properties** в соответствующем порядке
3. Для инициализации импорта файлов, введите в консоли команду `yii plans/import/init`

