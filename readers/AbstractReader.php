<?php
/**
 * Created by eaperesada.
 * Date: 29.05.2018
 * Time: 22:30
 * Email: ea.peresada@gmail.com
 * Phone: +7 (922) 339 60 49
 * VK: https://vk.com/peresada
 * Project: domrutest
 */

namespace perec\plans\readers;

/**
 * Base class for classes representing data-readers. Contains data and events
 *
 * @property mixed $reader
 * @property array $events
 * @property array $data
 * @property int $cacheSize
 * */
abstract class AbstractReader {
    /**
     * @var mixed $reader Repository for current reader
     * */
    protected $reader;
    
    /**
     * @var array $events Events for the current queue, added by Customer
     * */
    protected $events;
    
    /**
     * @var array $data Cache array
     * */
    protected $data;
    
    /**
     * @var int $cacheSize Allowed amount of elements in array $this->data
     * */
    protected $cacheSize = 1;
    
    /**
     * Event, triggers when current amount of elements in cache more then allowed cacheSize in @method [[pushData()]]
     * */
    const E_CACHE_OVERFLOW = "cacheOverflow";
    
    /**
     * Event, triggers when there are no more elements in document in @method [[pushData()]]
     * */
    const E_END = "readingFinished";
    
    /**
     * Main reading method, read full document
     * */
    abstract public function read();
    
    /**
     * Adding closure-function to the event of the current reader
     *
     * @param string|array $events
     * @param \Closure $callback
     *
     * @return $this
     * */
    public function on( $events , \Closure $callback ) {
        // Конечно правильней было использовать ивенты, встроенные в Yii,
        // но я вспомнил о них немного позже написания этих методов 0_0
        if ( is_array( $events ) ) {
            foreach ( $events as $event ) {
                $this->events[ $event ] = $callback;
            }
        } else {
            $this->events[ $events ] = $callback;
        }
        
        return $this;
    }
    
    /**
     * Adding element to cache
     *
     * @param mixed $content
     * @triggers "cacheOverflow"
     * */
    protected function pushData( $content ) {
        if ( $this->cacheOverflow() ) {
            $this->trigger(self::E_CACHE_OVERFLOW );
            $this->flush();
        }
        
        if ( $content ) {
            $this->data[] = $content;
        }
    }
    
    /**
     * Current amount of element in $this->data
     *
     * @return int
     * */
    public function currentCount() {
        return count( $this->data );
    }
    
    /**
     * Assert if current amount of $this->data element more then cacheSize
     *
     * @return bool
     * */
    public function cacheOverflow() {
        return $this->currentCount() >= $this->cacheSize;
    }
    
    /**
     * Setting cache size - amount of element in array $this->data
     *
     * @param int $value
     * */
    public function setCacheSize( int $value ) {
        $this->cacheSize = $value;
    }
    
    /**
     * Return cache data
     *
     * @return mixed
     * */
    public function fetch() {
        return $this->data;
    }
    
    /**
     * Erase data from cache
     * */
    public function flush() {
        $this->data = [];
    }
    
    /**
     * Trigger event from array $this->events
     *
     * @param string $eventName
     * @param array $params
     *
     * @return mixed
     * */
    public function trigger( string $eventName , array $params = [] ) {
        if ( ! isset( $this->events[$eventName] ) )
            return null;
            
        $params["context"] = $this;
        $closure = $this->events[ $eventName ];
        return call_user_func_array( $closure, $params );
    }
    
    
}