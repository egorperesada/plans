<?php
/**
 * Created by eaperesada.
 * Date: 29.05.2018
 * Time: 22:35
 * Email: ea.peresada@gmail.com
 * Phone: +7 (922) 339 60 49
 * VK: https://vk.com/peresada
 * Project: domrutest
 */

namespace perec\plans\readers;

/**
 * Class reader for XML-documents, using [[\XMLReader]]
 *
 * @example
 * ```php
 * $file = $this->module->params["config"]["files_dir"] . "/plans.xml";
 * $reader = new XMLReader($file);
 * $reader->setCacheSize(5000);
 * $model = new Plans();
 * $reader->on("targetFound", function ( $data ) use ( $model ) {
 *      $model->load($data, "");
 *      $result = $model->validate();
 *      return $result;
 * } );
 * $reader->on("cacheOverflow", function ($context) {
 *      print_r($context->fetch());
 * });
 * $reader->read();
 * ```
 *
 * @property string $targetNode
 * */
class XMLReader extends AbstractReader {
    /**
     * The name of the element that should be parsed in XML-document
     * */
    private $targetNode = "ROW";
    
    /**
     * Event, triggers when $targetNode was found in XML in @method [[read()]]
     * */
    const E_TARGET_FOUND = "targetFound";
    /**
     * Event, triggers after each iterate in @method [[read()]]
     * */
    const E_AFTER_ITERATE = "afterIterate";
    /**
     * Event, triggers before each iterate in @method [[read()]]
     * */
    const E_BEFORE_ITERATE = "beforeIterate";
    
    
    public function __construct( string $filePath ) {
        $this->reader = new \XMLReader();
        if ( file_exists( $filePath ) ) {
            $this->reader->open( $filePath );
        } else {
            \Yii::error("Файл " . $filePath . " не существует");
        }
    }
    
    /**
     * @inheritdoc
     * @triggers "targetFound", "afterIterate", "beforeIterate"
     * */
    public function read() {
        while ( $this->reader->read() ) {
            $this->trigger(self::E_BEFORE_ITERATE);
            if($this->reader->nodeType == \XMLReader::ELEMENT) {
                if ( $this->reader->localName == $this->targetNode ) {
                    $row = $this->reader->readOuterXml();
                    $row = $this->convertToArray( $row );
                    $result = $this->trigger(self::E_TARGET_FOUND, ["row" => $row]);
                    
                    if ( $result ) {
                        $this->pushData( $row );
                    }
                }
            }
            $this->trigger(self::E_AFTER_ITERATE);
        }
        
        $this->trigger(parent::E_END);
    }
  
    /**
     * Convert xml string to array
     *
     * @param string $xml
     * @return array
     * */
    private function convertToArray( string $xml ) {
        // Не самый привычный способ конвертации из xml в Массив,
        // но почему бы и нет? :)
        $xml = json_encode( simplexml_load_string($xml), JSON_UNESCAPED_UNICODE );
        $array = json_decode( $xml, true );
        $array = array_change_key_case( $array );
        
        return $array;
    }
    
    /**
     * Setting xml-element to be parsed in current queue
     *
     * @param string $name
     *
     * @return $this
     * */
    public function setTargetNode( string $name ) {
        $this->targetNode = $name;
        return $this;
    }
    
    public function __destruct() {
        $this->reader->close();
    }
}