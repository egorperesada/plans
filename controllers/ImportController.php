<?php

namespace perec\plans\controllers;

use perec\plans\models\MassLoad;
use perec\plans\services;
use perec\plans\models;

use perec\plans\readers\XMLReader;
use yii\console\Controller;


/**
 * Default controller for the `plansImporter` module
 */
// Я не добавлял логирование, ведение статистики, но
// Это безусловно стоит делать, если в задаче
// не говорится об обратном
class ImportController extends Controller {
    public function actionInit() {
        // По-хорошему надо выносить подобный код в отдельные классы,
        // чтобы не раздувать контроллеры, но для подобной задачи и маленького модуля
        // посчитал приемлимым позволять держать в контроллере больше кода
        $targetDir = $this->module->params[ "config" ][ "files_dir" ];
        
        $plansFile = $this->module->params[ "config" ][ "target_files" ]["plans"];
        $plans =  $targetDir . "/" . $plansFile;
        $this->processXml( new XMLReader( $plans ), new services\Plans( new models\Plans() ) );
        
        $propertiesFile = $this->module->params[ "config" ][ "target_files" ]["properties"];
        $properties =  $targetDir . "/" . $propertiesFile;
        $servicePlanProperties = new services\PlanProperties( new models\PlanProperties() );
        $this->processXml( new XMLReader( $properties ), $servicePlanProperties  );
        $servicePlanProperties->cleanConstraintFailed();
        echo "Загрузка данных завершена. В рамках этого тестового задания я не стал делать логирование и сбор статистики. Простите :(";
        return 1;
    }
    
    private function processXml( XMLReader $reader, MassLoad $model ) {
        $reader->setCacheSize( 5000 );
        $reader->on( $reader::E_TARGET_FOUND , function ( $data ) use ( $model ) {
            // Я не использую встроенную валидацию модели ActiveRecord потому что это значительно замедляет проверки,
            // для подобного рода задач (массовой загрузки) предпочитаю использовать собственные валидации, которые не требуют создания
            // дополнительных объектов, что увеличивает время работы скрипта и количество потребляемой памяти
            return $model->validateRow( $data );
        } );
        $reader->on( [ $reader::E_CACHE_OVERFLOW , $reader::E_END ] , function ( XMLReader $context ) use ( $model ) {
            $model->batch( $context->fetch() );
        } );
    
        $reader->read();
    }
    
}
