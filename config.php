<?php
/**
 * Created by eaperesada.
 * Date: 29.05.2018
 * Time: 21:49
 * Email: ea.peresada@gmail.com
 * Phone: +7 (922) 339 60 49
 * VK: https://vk.com/peresada
 * Project: domrutest
 */

return [
    "files_dir" => \Yii::$app->getModule("plans")->getBasePath() . "/data",
    "target_files" => [
        "plans" => "plans.xml",
        "properties" => "plan_properties.xml"]
];